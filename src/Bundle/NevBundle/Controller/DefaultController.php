<?php

namespace Bundle\NevBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NevBundle:Default:index.html.twig');
    }
}
